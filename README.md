# rppico-upython

Contains some MicroPython projects for Raspberry Pi Pico (W).

daylight_alarm.py is an alarm clock intended to simulate sunrise. It runs on the Pico W and is configured via your web browser.